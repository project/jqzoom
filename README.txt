Drupal jQZoom module:
--------------------
Author - Matt Vance (mvance at pobox dot com)
Requires - Drupal 6
License - GPL


Overview:
--------
The jQZoom module is a wrapper for the jQuery plugin jQZoom.
The jQuery library is a part of Drupal since version 5.

* jQuery - http://jquery.com/
* jQZoom - http://www.mind-projects.it/projects/jqzoom/

From the jQZoom homepage:
"jQZoom allows you to realize a small magnifier window close to the image or
images on your web page easily. I decided to build this jQuery plugin to embed
detailed big images in my B2B. So now in few steps you can have your jQZoom
in your website,eCommerce or whatever you want."


Installation:
------------
1) Start by copying the module files into your 'modules' directory 
(usually /sites/all/modules)

2) Download the jQZoom files from:
http://www.mind-projects.it/blog/jqzoom_v10

3) Copy the js, css, and images directories from the jQZoom archive file into
the jqzoom directory.

4) On the Administer > Site building > Modules page, enable the jQZoom module.

5) The preferred method for using jQZoom is in conjunction with the ImageCache
module. If not already installed, download, enable, and configure the latest
versions of the CCK, FileField, ImageField, ImageAPI, and Transliteration
modules. NOTE: The Drupal 5 version of jQZoom also worked with the Image module;
however, the Drupal 6 version has not yet been tested in that configuration.

6) Enable an image toolkit module (either ImageAPI GD2 or ImageAPI ImageMagick).

7) Select Site building > Imagecache > Add new preset. Specify a namespace.
On the following screen, specify an action, such as "Add Scale" and specify
the desired transformation settings.

8) Add an image field to a content typs. For example, select Content
management > Content types > Edit story > Manage fields. Specify a label,
field name, "File" as the Type of Data Stored, and "Image" as the Form element
to edit the data. On the next screen, leave the defaults and click on "Save
field settings."

9) Select Content management > Content types > Edit story > Display fields.
For either Teaser or Full node view, specify the desired jQZoom/ImageCache
preset that you want to use when displaying the image.

Configuration:
-------------
As said on the jQZoom homepage:
"Using jQZoom is easy,but you need to specify the HTML anchor element,that is 
going to generate the zoom revealing a portion of the enlarged image.

<a href="images/BIGIMAGE.JPG" class="MYCLASS"  title="MYTITLE">
<img src="images/SMALLIMAGE.JPG" title="IMAGE TITLE" />
</a>

The anchor element wraps the small image you would like to zoom. Following 
this schema the necessary and base elements are:
SMALLIMAGE.JPG: Represents the small image you would like to zoom.
BIGIMAGE.JPG: Represents the big image that jQZoom will reveal.
MYCLASS: Represents the anchor class,that would be used to instantiate the 
jQZoom script to all the elements matching this class(you can use an ID as
well). MYTITLE/IMAGE TITLE: Anchor title and/or image title that will be used
to show the zoom title close to the jQZoom Window."

Contributions:
-------------
* This module is largely based on the Thickbox module. Many thanks to all
  the Thickbox developers and contributors.

Last updated:
------------
